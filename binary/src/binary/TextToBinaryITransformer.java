/**
 * 
 */
package binary;

import java.math.BigInteger;

/**
 * @author babbleshack
 *
 */
public class TextToBinaryITransformer implements ITransformer {
	
	private byte[] bytes;
	private byte val;
	private StringBuilder binary;
	/*
	 * (non-Javadoc)
	 * @see binary.ITransformer#transform(java.lang.String)
	 */
	@Override
	public String transform(String text) {
		//clear variables
		bytes = null;
		binary = new StringBuilder();
		bytes = text.getBytes();
		for (byte b: bytes)
		{
			val = b;
			for (int i = 0; i < 8; i++)
		     {
		        binary.append((val & 128) == 0 ? 0 : 1);
		        val <<= 1;
		     }
		}
		return binary.toString();
	}

}
