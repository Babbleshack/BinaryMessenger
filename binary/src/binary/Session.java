/**
 * 
 */
package binary;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

/**
 * @author babbleshack
 *
 */
public class Session {
	private MutableDateTime sessionDateTime;
	private ArrayList<Message> messages;
	/*
	 * innitialises session setting datatime to current time.
	 */
	public Session(){
		this.sessionDateTime = new MutableDateTime();
		this.messages = new ArrayList<>();
	}
	public Session(int temp/*gonna take d/m/y hh:mm:ss data from sql*/) {
	}
	public MutableDateTime getSessionDateTime() {
		return sessionDateTime;
	}
	public void setSessionDateTime(MutableDateTime sessionDateTime) {
		this.sessionDateTime = sessionDateTime;
	}
	public ArrayList<Message> getMessages() {
		return messages;
	}
	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}
	public void addMessage(Message message) {
		this.messages.add(message);
	}
	public Message getLatestMessage(){
		return messages.get(messages.size() - 1);
		
	}
	
	
}
