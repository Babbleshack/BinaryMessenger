/**
 * 
 */
package binary;

/**
 * @author babbleshack
 *
 */
public class Message {
	private String textMessage;
	private String altMessage;
	public String getTextMessage() {
		return textMessage;
	}
	public void setTextMessage(String textMessage) {
		this.textMessage = textMessage;
	}
	public String getAltMessage() {
		return altMessage;
	}
	public void setAltMessage(String altMessage) {
		this.altMessage = altMessage;
	}
	

}
