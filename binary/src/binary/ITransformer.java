package binary;

public interface ITransformer {
	public String transform(String text);
}
