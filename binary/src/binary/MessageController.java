/**
 * 
 */
package binary;

/**
 * @author babbleshack
 *
 */
public class MessageController {
	private View view;	
	private MessageRepository repository;
	
	/*
	 * to be innitialised from driver
	 * Message Controller con
	 * MessageRepository messageRepository - repository
	 * MessageModel model - model passed to constructor
	 * 
	 */
	public MessageController(MessageRepository messageRepository, MessageModel model) {
		this.repository = messageRepository;
		this.repository.setModel(model);
	}
	public void setView(View view) {
		this.view = view;
	}
	
	public void createSession(){
		
	}
	public void createMessage(String message){
		
		this.repository.createMessage(message);
		//this.view.notifyAll();
		
	}

}
