package binary;

import javax.swing.SwingUtilities;

public class Driver {

	public static void main(String[] args) {
		MessageModel model = new MessageModel();
		MessageController controller = new MessageController(new MessageRepository(), model);
		final View view = new View(controller);
		model.addObserver(view);
		controller.setView(view);
		
		SwingUtilities.invokeLater(new Runnable() {
	         public void run() {
	            View.createAndShowGUI();  // Let the constructor do the job
	         }
	      });
		// TODO Auto-generated method stub

	}

}
