/**
 * 
 */
package binary;

/**
 * @author babbleshack
 *
 */
public class MessageRepository {
	private MessageModel model;
	private ITransformer transformer;
	private IMessageDatabaseConnection MessageDatabase;
	
	public void setModel(MessageModel model) {
		this.model = model;
		this.transformer = new TextToBinaryITransformer();
	}
	public void createSession(){
		Session session = new Session();
		model.addSession(session);
		setCurrentSession(session);
	}
	public void setCurrentSession(Session session)
	{
		model.setSession(session);
	}
	/*
	 * creates a new message object which is stored in a session object.
	 * if no current session is set at the model, a new sessions is created.
	 * @param text -String user message.
	 * @return void
	 */
	public void createMessage(String text){
		if(model.getCurrentSession() == null){
			this.createSession();
		}
		Message message = new Message();
		message.setTextMessage(text);
		message.setAltMessage(transformer.transform(text));
		this.addMessage(message);
		//this.model.notifyObservers(message);
	}
	public void addMessage(Message message)
	{
		model.addMessage(message);
	}
	public Message getLastMessage(){
		return model.getCurrentSession().getLatestMessage();
	}

}
