package binary;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;



public class View implements Observer {
	
	private static MessageController controller;
	


	public static JFrame frame;
	
	private static final int TEXTFIELDWIDTH = 64; 
	private static final String DEFAULTREADERTEXT = "Please Enter Your Message";
	private static final String NEWLINE = "\n";
	
	//components
	//message displayer panel
	private static JPanel messengerPanel; 
	private static JLabel instructionLabel;
	private static JScrollPane scrollPane;
	private static JTextArea messengerTextArea;
	
	//message reader panel
	private static JTextField messageReader;
	
	
	//constraints
	//messenger panel constraints
	private static GridBagConstraints instructionConstraints;
	private static GridBagConstraints scrollConstraints;
	private static GridBagConstraints messageReaderConstraints;
	
	
	private static String[] messages = {
            "Hello there\n",
            "How you doing ?\n",
            "This is a very long text that might won't fit in a single line :-)\n",
            "Okay just to occupy more space, it's another line.\n",
            "Don't read too much of the messages, instead work on the solution.\n",
            "Byee byee :-)\n",
            "Cheers\n"
        };
	public View(MessageController controller){
		View.controller = controller;
	}
	public static void setController(MessageController controller) {
		View.controller = controller;
	}
	
	private static Container createMessengerPanel(){
		//instruction label
		instructionLabel = new JLabel();
		instructionConstraints = new GridBagConstraints();
		instructionLabel.setText("Please enter your message.");
		instructionConstraints.gridx = 1;
		instructionConstraints.gridy = 0;
		instructionConstraints.gridheight = 3;
		instructionConstraints.gridwidth = 3;
		instructionConstraints.ipady = 50;
		instructionConstraints.weightx = 1;
		instructionConstraints.weighty = 0.1;
		instructionConstraints.anchor = GridBagConstraints.PAGE_START;
		
		//message display area
		messengerPanel = new JPanel(new GridBagLayout());
		scrollPane = new JScrollPane();
		messengerTextArea = new JTextArea();
		scrollConstraints = new GridBagConstraints();
		messengerTextArea.setEditable(false);
		messengerTextArea.setWrapStyleWord(true);
		messengerTextArea.setLineWrap(true);
		//add message area to scrollpane
		scrollPane.add(messengerTextArea);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Messages"));
		scrollPane.setViewportView(messengerTextArea);		
		scrollConstraints.gridx = 1;
		scrollConstraints.gridy = 1;
		scrollConstraints.gridheight = 1;
		scrollConstraints.gridwidth = 3;
		scrollConstraints.fill = GridBagConstraints.HORIZONTAL;
		scrollConstraints.ipady = 100;
		scrollConstraints.weightx = 1;
		scrollConstraints.weighty = 1;
		scrollConstraints.anchor = GridBagConstraints.CENTER;
		
		//message reader panel
		messageReader = new JTextField(DEFAULTREADERTEXT, TEXTFIELDWIDTH);
		messageReader.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				messageReader.setText("");
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				messageReader.setText(DEFAULTREADERTEXT);
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				messageReader.setText("");
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				//messageReader.setText(DEFAULTREADERTEXT);
				
			}
			
		});
		messageReader.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				controller.createMessage(messageReader.getText());
				//View.appendMessage(messageReader.getText());
				messageReader.setText("");
				//messageReader.setText(DEFAULTREADERTEXT);
			}
			
		});
		messageReaderConstraints = new GridBagConstraints();
		messageReaderConstraints.gridx = 0;
		messageReaderConstraints.gridy = 2;
		messageReaderConstraints.gridheight = 1;
		messageReaderConstraints.gridwidth = GridBagConstraints.REMAINDER;
		messageReaderConstraints.fill = GridBagConstraints.HORIZONTAL;
		messageReaderConstraints.weightx = 1;
		messageReaderConstraints.weighty = 1;
		
		
		//add to panel
		//messengerPanel.add(instructionLabel, instructionConstraints);
		messengerPanel.add(scrollPane, scrollConstraints);
		messengerPanel.add(messageReader, messageReaderConstraints);
		
		return messengerPanel;
	}
	/*
	 * instantiates GUI controls
	 * @param void
	 * @return void
	 */
	public static void createAndShowGUI(){
		frame = new JFrame();
		frame.setContentPane(createMessengerPanel());
		frame.setSize(new Dimension(500, 500));
		frame.setTitle("Please Enter Your Message!");
		frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	/*
	 * prints message to MessageArea.
	 * @param message string containing message.
	 * @return void.
	 */
	public static void appendMessage(String message){
		messengerTextArea.append(message + NEWLINE);
	}
	
	
	public void update(Observable arg0, Object arg1) {
		View.appendMessage(((Message)arg1).getAltMessage());
	}

	

}
