package binary;

import java.util.ArrayList;
import java.util.Observable;

public class MessageModel extends Observable {
	private ArrayList<Session> sessions;
	private Session currentSession;

	public Session getCurrentSession() {
		return currentSession;
	}
	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}
	public ArrayList<Session> getSessions() {
		return sessions;
	}
	public void setSession(Session session){
		this.currentSession = session;
	}
	public void setSessions(ArrayList<Session> sessions) {
		this.sessions = sessions;
	}
	public void addMessage(Message message){
		this.currentSession.addMessage(message);
		this.setChanged();
		this.notifyObservers(message);
	}
	public void addSession(Session session) {
		// TODO Auto-generated method stub
		
	}
	
	
}
